package com.example.savestatehatamkotlin.modelclass

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    private val count: MutableLiveData<Int> = MutableLiveData()
    val liveCount: LiveData<Int>
        get() = count

    init {
        count.value = 0
    }

    fun increase() {
        count.value = count.value!! + 1

    }

    fun decrease() {
        count.value = count.value!! - 1

    }

}