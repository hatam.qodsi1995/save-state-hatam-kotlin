package com.example.savestatehatamkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.savestatehatamkotlin.modelclass.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textCount = findViewById<View>(R.id.number_place) as TextView
        val plusBtn = findViewById<View>(R.id.plusBtn)
        val minusBtn = findViewById<View>(R.id.minusBtn)

        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        viewModel.liveCount.observe(this, {
            textCount.text = it.toString()
        })

        plusBtn.setOnClickListener {
            viewModel.increase()
        }

        minusBtn.setOnClickListener {
            viewModel.decrease()
        }
    }

}